#ifndef BITMAP_H
#define BITMAP_H

#include <vector>
#include "pixel.h"
#include "bitmapheader.h"

class bitmap{
    public:
        bitmap(char *file);
		bitmap(int h, int w);
		//bitmap();
		static bool fileworks(char *file);
		std::vector<std::vector<pixel>> getmyPixels();
        void setmyPixels(std::vector<std::vector<pixel>>);
        bool save(char *file);
    private:
        vector<vector<pixel>> myPixels;
};

#endif