#ifndef PIXEL_H
#define PIXEL_H

#include "color.h"
#include <cmath>

class pixel{
    public:
        pixel(int,int,color*);
		pixel();
        int myX;
        int myY;
        color* getColor();
		void setBLACK();
        void setColor(color*);
        int magnitude(color*);
        bool equal(color*);
    private:
        color* myColor;
};

#endif