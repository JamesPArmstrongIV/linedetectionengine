#ifndef K_CPP
#define K_CPP

#include <math.h>
#include <vector>

struct point3{
    int x;
    int y;
    int z;
};

int mag(point3* comp1, point3* comp2){
    return  sqrt(
        pow((double)(comp1->x-comp2->x),2)
        +pow((double)(comp1->y-comp2->y),2)
        +pow((double)(comp1->z-comp2->z),2)
    );
}



#endif