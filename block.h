#ifndef BLOCK_H
#define BLOCK_H
#include <vector>
#include "pixel.h"

using namespace std;
class block {
    public:
        block(pixel p);
		int magnitude;
		pixel firstPixel;
        vector<pixel>* myPixels;
        color* myColor;
		int myNumberOfPixels;

        bool addPixel(pixel);
		color* getColor();
		void setBlockColor();
		int incrementNumber();
		int getNumberOfPixels();
		vector<pixel> getPixels();
};

#endif