#ifndef BITMAPHEADER_CPP
#define BITMAPHEADER_CPP

#include <iostream>
#include <fstream>

#include "bitmapheader.h"

unsigned short BITMAPFILEHEADER::GETbfType(){
	return bfType;
}

unsigned long int   BITMAPFILEHEADER::GETbfSize(){
	return bfSize;
}

int  BITMAPFILEHEADER::ReadBmpFileHeader (ifstream &fp){
	fp.read ((char*)&bfType, sizeof(bfType));
	fp.read ((char*)&bfSize, sizeof(bfSize));
	fp.read ((char*)&bfReserved1, sizeof(bfReserved1));
	fp.read ((char*)&bfReserved2, sizeof(bfReserved2));
	fp.read ((char*)&bfOffBits, sizeof(bfOffBits));
	return 1;
}

int  BITMAPFILEHEADER::WriteBmpFileHeader (ofstream &fp){
	fp.write ((char*)&bfType, sizeof(bfType));
	fp.write ((char*)&bfSize, sizeof(bfSize));
	fp.write ((char*)&bfReserved1, sizeof(bfReserved1));
	fp.write ((char*)&bfReserved2, sizeof(bfReserved2));
	fp.write ((char*)&bfOffBits, sizeof(bfOffBits));
	return 1;
}
int  BITMAPINFOHEADER::GETbiWS(){
	return biWS;
}
int  BITMAPINFOHEADER::GETbiHS(){
	return biHS;
}
unsigned short int BITMAPINFOHEADER::GETbiBitCount(){
	return biBitCount;
}
int  BITMAPINFOHEADER::ReadBmpInfoHeader (ifstream &fp) {
	fp.read ((char*)&biSize, sizeof(biSize));
	fp.read ((char*)&biWS, sizeof(biWS));
	fp.read ((char*)&biHS, sizeof(biHS));
	fp.read ((char*)&biPlanes, sizeof(biPlanes));
	fp.read ((char*)&biBitCount, sizeof(biBitCount));
	fp.read ((char*)&biCompression, sizeof(biCompression));
	fp.read ((char*)&biSZ, sizeof(biSZ));
	fp.read ((char*)&biXPelsPerMeter, sizeof(biXPelsPerMeter));
	fp.read ((char*)&biYPelsPerMeter, sizeof(biYPelsPerMeter));
	fp.read ((char*)&biClrUsed, sizeof(biClrUsed));
	fp.read ((char*)&biClrImportant,  sizeof(biClrImportant));
	return 1;
}
int  BITMAPINFOHEADER::WriteBmpInfoHeader (ofstream &fp){
	fp.write ((char*)&biSize, sizeof(biSize));
	fp.write ((char*)&biWS, sizeof(biWS));
	fp.write ((char*)&biHS, sizeof(biHS));
	fp.write ((char*)&biPlanes, sizeof(biPlanes));
	fp.write ((char*)&biBitCount, sizeof(biBitCount));
	fp.write ((char*)&biCompression, sizeof(biCompression));
	fp.write ((char*)&biSZ, sizeof(biSZ));
	fp.write ((char*)&biXPelsPerMeter, sizeof(biXPelsPerMeter));
	fp.write ((char*)&biYPelsPerMeter, sizeof(biYPelsPerMeter));
	fp.write ((char*)&biClrUsed, sizeof(biClrUsed));
	fp.write ((char*)&biClrImportant,  sizeof(biClrImportant));
	return 1;
}

#endif