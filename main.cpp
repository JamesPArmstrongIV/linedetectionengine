#ifndef MAIN_CPP
#define MAIN_CPP

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <ctgmath>
#include <random>

using namespace std;

/**/
class BITMAPFILEHEADER               /**** BMP file header structure ****/
{
private:
	unsigned short bfType;           /* Magic number for file */
	unsigned int   bfSize;           /* Size of file */
	unsigned short bfReserved1;      /* Reserved */
	unsigned short bfReserved2;      /* ... */
	unsigned int   bfOffBits;        /* Offset to bitmap data */
public:
	unsigned short GETbfType();
	unsigned long int   GETbfSize();
	int  ReadBmpFileHeader(ifstream &);
	int  WriteBmpFileHeader(ofstream &);
};

#  define BF_TYPE 0x4D42                   /* "MB" */

class  BITMAPINFOHEADER      /**** BMP file info structure ****/
{
private:
	unsigned int   biSize;            /* Size of info header */
	int            biWS;              /* Width of image */
	int            biHS;              /* Height of image */
	unsigned short int biPlanes;      /* Number of color planes */
	unsigned short int biBitCount;    /* Number of bits per pixel */
	unsigned int   biCompression;     /* Type of compression to use */
	unsigned int   biSZ;              /* Size of image data */
	int            biXPelsPerMeter;   /* X pixels per meter */
	int            biYPelsPerMeter;   /* Y pixels per meter */
	unsigned int   biClrUsed;         /* Number of colors used */
	unsigned int   biClrImportant;    /* Number of important colors */
public:
	int            GETbiWS();
	int            GETbiHS();
	unsigned short int GETbiBitCount();
	int  ReadBmpInfoHeader(ifstream &);
	int  WriteBmpInfoHeader(ofstream &);
};

unsigned short BITMAPFILEHEADER::GETbfType() {
	return bfType;
}

unsigned long int   BITMAPFILEHEADER::GETbfSize() {
	return bfSize;
}

int  BITMAPFILEHEADER::ReadBmpFileHeader(ifstream &fp) {
	fp.read((char*)&bfType, sizeof(bfType));
	fp.read((char*)&bfSize, sizeof(bfSize));
	fp.read((char*)&bfReserved1, sizeof(bfReserved1));
	fp.read((char*)&bfReserved2, sizeof(bfReserved2));
	fp.read((char*)&bfOffBits, sizeof(bfOffBits));
	return 1;
}

int  BITMAPFILEHEADER::WriteBmpFileHeader(ofstream &fp) {
	fp.write((char*)&bfType, sizeof(bfType));
	fp.write((char*)&bfSize, sizeof(bfSize));
	fp.write((char*)&bfReserved1, sizeof(bfReserved1));
	fp.write((char*)&bfReserved2, sizeof(bfReserved2));
	fp.write((char*)&bfOffBits, sizeof(bfOffBits));
	return 1;
}
int  BITMAPINFOHEADER::GETbiWS() {
	return biWS;
}
int  BITMAPINFOHEADER::GETbiHS() {
	return biHS;
}
unsigned short int BITMAPINFOHEADER::GETbiBitCount() {
	return biBitCount;
}
int  BITMAPINFOHEADER::ReadBmpInfoHeader(ifstream &fp) {
	fp.read((char*)&biSize, sizeof(biSize));
	fp.read((char*)&biWS, sizeof(biWS));
	fp.read((char*)&biHS, sizeof(biHS));
	fp.read((char*)&biPlanes, sizeof(biPlanes));
	fp.read((char*)&biBitCount, sizeof(biBitCount));
	fp.read((char*)&biCompression, sizeof(biCompression));
	fp.read((char*)&biSZ, sizeof(biSZ));
	fp.read((char*)&biXPelsPerMeter, sizeof(biXPelsPerMeter));
	fp.read((char*)&biYPelsPerMeter, sizeof(biYPelsPerMeter));
	fp.read((char*)&biClrUsed, sizeof(biClrUsed));
	fp.read((char*)&biClrImportant, sizeof(biClrImportant));
	return 1;
}
int  BITMAPINFOHEADER::WriteBmpInfoHeader(ofstream &fp) {
	fp.write((char*)&biSize, sizeof(biSize));
	fp.write((char*)&biWS, sizeof(biWS));
	fp.write((char*)&biHS, sizeof(biHS));
	fp.write((char*)&biPlanes, sizeof(biPlanes));
	fp.write((char*)&biBitCount, sizeof(biBitCount));
	fp.write((char*)&biCompression, sizeof(biCompression));
	fp.write((char*)&biSZ, sizeof(biSZ));
	fp.write((char*)&biXPelsPerMeter, sizeof(biXPelsPerMeter));
	fp.write((char*)&biYPelsPerMeter, sizeof(biYPelsPerMeter));
	fp.write((char*)&biClrUsed, sizeof(biClrUsed));
	fp.write((char*)&biClrImportant, sizeof(biClrImportant));
	return 1;
}
/**/



struct color {
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

int mag(color* c1, color* c2) {
	return sqrt(pow(c1->b-c2->b,2)+
		pow(c1->r-c2->r,2)+
		pow(c1->g-c2->g,2));
}

bool equals(color* c1, color* c2) {
	if (mag(c1, c2) == 0) return true;
	return false;
}

void black(color** c) {
	(*c)->r = 0;
	(*c)->g = 0;
	(*c)->b = 0;
}

int nearest(vector<int> x,vector<int> y,vector<int> z,color* c) {
	int r = (int)c->r;
	int g = (int)c->g;
	int b = (int)c->b;
	int n = -1;
	int a = 100000;
	int temp = 0;
	color* t = new color();
	for (int i = 0; i < x.size(); i++) {
		t = new color;
		t->r = x[i];
		t->b = y[i];
		t->g = z[i];
		temp = mag(c, t);
		if (temp < a) {
			n = i;
			a = temp;
		}
		temp = 0;
	}
	return n;
}


void block(vector<color*>* c, int w, int h,int k){
	vector<int> x(k);
	vector<int> y(k);
	vector<int> z(k);
	vector<int> in(k);
	for (int i = 0; i < k; i++) {
		x[i] = rand() % 256;
		y[i] = rand() % 256;
		z[i] = rand() % 256;
	}
	vector<int> near((*c).size());
	bool watcher = true;
	int v = 0;
	while (true) {
		cout << "loop number " << v << endl;
		v++;
		watcher = true;
		for (int i = 0; i < (*c).size(); i++) {
			int catcher = near[i];
			near[i] = nearest(x, y, z, (*c)[i]);
			if (catcher != near[i]) {
				watcher = false;
			}
		}
		if (watcher)	break;
	}
	vector<color*> j(k);
		for (int i = 0; i < k; i++) {
			in[i] = 1;
			x[i] = 0;
			y[i] = 0;
			z[i] = 0;
		}
		for (int i = 0; i < (*c).size(); i++) {
			in[near[i]]++;
			x[near[i]] = x[near[i]] + (int)(*c)[i]->r;
			y[near[i]] = y[near[i]] + (int)(*c)[i]->b;
			z[near[i]] = z[near[i]] + (int)(*c)[i]->g;
		}
		for (int i = 0; i < k; i++) {
			x[i] = x[i] / in[i];
			y[i] = y[i] / in[i];
			z[i] = z[i] / in[i];
			j[i] = new color();
			j[i]->r = x[i];
			j[i]->g = z[i];
			j[i]->b = y[i];
		}
		vector<color*> out = *c;
		for (int i = 0; i < (*c).size(); i++) {
			 out[i] = j[near[i]];
		}
		*c = out;
}

int main() {
	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bhd;

	cout << "File to load?";
	string file;
	cin >> file;

	ifstream fp1(file+".bmp", ios::in | ios::binary);
	if (!fp1.is_open()) {
		cout << "Usage: " << "CAT" << " <input_filename> <output_filename>" << endl;
		return 1;
	}

	cout << "File to save?" << endl;
	string arg;
	cin >> arg;

	cout << arg << ".bmp" << endl;
	ofstream fp2(arg+".bmp", ios::out | ios::binary);
	if (!fp2.is_open()) {
		cout << "Usage: " << "CAT" << " <input_filename> <output_filename>" << endl;
		return 1;
	}

	int  success = 0;
	success = bfh.ReadBmpFileHeader(fp1);
	if (!success)
	{
		/* Couldn't read the file header - return NULL... */
		fp1.close();
		return -1;
	}


	if (bfh.GETbfType() != BF_TYPE)  /* Check for BM reversed, ie MB... */
	{
		cout << "ID is: " << bfh.GETbfType() << " Should have been" << 'M' * 256 + 'B';
		cout << bfh.GETbfType() / 256 << " " << bfh.GETbfType() % 256 << endl;
		/* Not a bitmap file - return NULL... */
		fp1.close();
		return 1;
	}


	cout << "Image data Size: " << bfh.GETbfSize() << endl;

	success = 0;
	success = bhd.ReadBmpInfoHeader(fp1);
	if (!success)
	{
		/* Couldn't read the file header - return NULL... */
		fp2.close();
		return -1;
	}

	cout << "Image Width Size:  " << bhd.GETbiWS() << endl;
	cout << "Image Height Size:  " << bhd.GETbiHS() << endl;
	cout << "Bitcount: " << bhd.GETbiBitCount() << endl;

	bfh.WriteBmpFileHeader(fp2);
	bhd.WriteBmpInfoHeader(fp2);
	int w = bhd.GETbiWS();
	int h = bhd.GETbiHS();
	vector<color*> c(w*h);

	unsigned char r, b, g;
	color* temp = new color();

	for (int i = 0; i < bhd.GETbiWS(); i++)
		for (int j = 0; j < bhd.GETbiHS(); j++) {
			int k = i * h + j;
			temp = new color();
			fp1.read((char*)&b, 1);
			fp1.read((char*)&g, 1);
			fp1.read((char*)&r, 1);
			temp->r = r;
			temp->b = b;
			temp->g = g;
			c[k] = temp;
		}

	for (int i = 0; i < bhd.GETbiWS(); i++)
		for (int j = 0; j < bhd.GETbiHS(); j++) {
			int k = i * h + j;
			temp = new color();
			temp = c[k];
			r = temp->r;
			b = temp->b;
			g = temp->g;
			fp2.write((char*)&b, sizeof(char));
			fp2.write((char*)&g, sizeof(char));
			fp2.write((char*)&r, sizeof(char));
		}

	cout << "How many objects?" << endl;
	int k = 0;
	cin >> k;

	block(&c,w,h,k);
	
	ofstream fp3(arg + "block.bmp", ios::out | ios::binary);
	if (!fp3.is_open()) {
		cout << "Usage: " << "CAT" << " <input_filename> <output_filename>" << endl;
		return 1;
	}

	bfh.WriteBmpFileHeader(fp3);
	bhd.WriteBmpInfoHeader(fp3);

	for (int i = 0; i < bhd.GETbiWS(); i++)
		for (int j = 0; j < bhd.GETbiHS(); j++) {
			int k = i * h + j;
			temp = new color();
			temp = c[k];
			r = temp->r;
			b = temp->b;
			g = temp->g;
			fp3.write((char*)&b, sizeof(char));
			fp3.write((char*)&g, sizeof(char));
			fp3.write((char*)&r, sizeof(char));
		}

	//line is called

	ofstream fp4(arg + "line.bmp", ios::out | ios::binary);
	if (!fp4.is_open()) {
		cout << "Usage: " << "CAT" << " <input_filename> <output_filename>" << endl;
		return 1;
	}

	bfh.WriteBmpFileHeader(fp4);
	bhd.WriteBmpInfoHeader(fp4);

	for (int i = 0; i < bhd.GETbiWS(); i++)
		for (int j = 0; j < bhd.GETbiHS(); j++) {
			int k = i * h + j;
			temp = new color();
			temp = c[k];
			r = temp->r;
			b = temp->b;
			g = temp->g;
			fp4.write((char*)&b, sizeof(char));
			fp4.write((char*)&g, sizeof(char));
			fp4.write((char*)&r, sizeof(char));
		}

	fp1.close();
	fp2.close();
	fp3.close();
	fp4.close();
	return 0;
}

#endif