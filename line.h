#ifndef LINE_H
#define LINE_H

#include <vector>

#include "block.h"
#include "bitmap.h"

using namespace std;
class line{
    public:
        line(bitmap painting, bitmap og);
		bitmap getLines();
		bitmap painting;
		bitmap og;
		vector < vector<pixel>> image;
		vector < vector<pixel>> lines;
};

#endif